variable "gitlab_token" {
  description = "A Gitlab Personal Access token"
  sensitive   = true
  type        = string
}

variable "gitlab_group_path" {
  description = "Path to your Gitlab Group"
  type        = string
}

variable "instance_type" {
  description = "EC2 Instance type of the runner"
  default = "t4g.micro"
}
